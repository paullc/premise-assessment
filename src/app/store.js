import { configureStore } from '@reduxjs/toolkit';
import wizardStepperReducer from '../features/wizardStepper/wizardStepperSlice'
import menuPageReducer from '../features/menuPage/menuPageSlice'
import pokemonSearchPageReducer from '../features/pokemonSearchPage/pokemonSearchPageSlice'
import pokemonInfoPageSliceReducer from "../features/pokemonInfoPage/pokemonInfoPageSlice";

export const store = configureStore({
  reducer: {
    wizardStepper: wizardStepperReducer,
    menuPage: menuPageReducer,
    pokemonSearchPage: pokemonSearchPageReducer,
    pokemonInfoPage: pokemonInfoPageSliceReducer
  },
});