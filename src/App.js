import React from 'react';
import './App.css';
import {Wizard} from "./features/wizard/Wizard";

function App() {
  return (
    <div className="App">
      <Wizard/>
    </div>
  );
}

export default App;
