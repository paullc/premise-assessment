import * as React from "react";
import WizardStepper from "../wizardStepper/WizardStepper";

// This component encompasses the entire app
export function Wizard() {
  return (
    <div>
      <header style={{ paddingTop: 25, fontSize: 50 }}>
        Pau's Pokémon Pocket-Wizard!
      </header>

      <WizardStepper />
    </div>
  );
}
