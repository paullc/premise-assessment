import { createAsyncThunk, createSlice } from "@reduxjs/toolkit";
import axios from "axios";

const initialState = {
  info: {},
  status: "idle",
  error: null,
};

// An async thunk used to grab a specific pokemon. The API accepts either the undercase pokemon name or its registration
// number, so both are accepted here.
export const fetchPokemon = createAsyncThunk(
  "pokemonInfoPage/fetchPokemon",
  async (pokemon) => {
    return await axios
      .get(`https://pokeapi.co/api/v2/pokemon/${pokemon}`)
      .then((response) => response.data)
      .catch((error) => error);
  }
);

export const pokemonInfoPageSlice = createSlice({
  name: "pokemonInfoPage",
  initialState, // The `reducers` field lets us define reducers and generate associated actions
  reducers: {
    newSearch: (state, action) => {
      state.status = "idle";
    },
  },
  extraReducers: (builder) => {
    builder
      .addCase(fetchPokemon.pending, (state, action) => {
        state.status = "loading";
      })
      .addCase(fetchPokemon.fulfilled, (state, action) => {
        state.status = "succeeded";
        // Add any fetched posts to the array
        state.info = action.payload;
      })
      .addCase(fetchPokemon.rejected, (state, action) => {
        state.status = "failed";
        state.error = action.error.message;
      });
  },
});

// Selectors
export const selectPokemonInfo = (state) => state.pokemonInfoPage.info;
export const selectFetchStatus = (state) => state.pokemonInfoPage.status;
export const selectError = (state) => state.pokemonInfoPage.error;

// Actions

export const { newSearch } = pokemonInfoPageSlice.actions;

// Reducer

export default pokemonInfoPageSlice.reducer;
