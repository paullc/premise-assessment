import pokemonInfoPageReducer, { newSearch } from "./pokemonInfoPageSlice";

describe("pokemon info page reducer", () => {
  const initialState = {
    info: {},
    status: "idle",
    error: null,
  };

  it("should handle initial state", () => {
    expect(pokemonInfoPageReducer(undefined, { type: "unknown" })).toEqual({
      info: {},
      status: "idle",
      error: null,
    });
  });

  it("should handle newSearch", () => {
    const actual = pokemonInfoPageReducer(
      {
        info: {},
        status: "succeeded",
        error: null,
      },
      newSearch()
    );
    expect(actual.status).toEqual("idle");
  });
});
