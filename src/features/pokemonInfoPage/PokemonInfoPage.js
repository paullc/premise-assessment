import { useDispatch, useSelector } from "react-redux";
import React, { useEffect } from "react";
import {
  CircularProgress,
  Alert,
  AlertTitle,
  Card,
  CardHeader,
  CardMedia,
  CardContent,
  Typography,
  Grid,
} from "@mui/material";

import {
  fetchPokemon,
  selectFetchStatus,
  selectPokemonInfo,
  selectError,
} from "./pokemonInfoPageSlice";
import { selectChosenPokemon } from "../pokemonSearchPage/pokemonSearchPageSlice";
import { selectSelectedOption } from "../menuPage/menuPageSlice";

// The info page contains all the information about the currently selected pokemon
export function PokemonInfoPage() {
  const dispatch = useDispatch();

  // Information pertaining to the pokemon
  const pokemonInfo = useSelector(selectPokemonInfo);

  // Status of the action
  const status = useSelector(selectFetchStatus);

  // Pokemon that the user has chosen or that the randomizer has chosen
  const chosenPokemon = useSelector(selectChosenPokemon);

  // Error message of an action if it is faulty
  const error = useSelector(selectError);

  // Selected menu option
  const menuOption = useSelector(selectSelectedOption);

  // Max number of different pokemon; used as a cap for the randomizer
  const maxPokemonNumber = 898;

  // Random integer between two integers; to pick a random pokemon by its registration number
  function getRandomInt(min, max) {
    min = Math.ceil(min);
    max = Math.floor(max);
    return Math.floor(Math.random() * (max - min) + min); //The maximum is exclusive and the minimum is inclusive
  }

  // Query the Pokemon API for either a specific user-searched pokemon or a randomly indexed pokemon
  useEffect(() => {
    if (status === "idle") {
      if (menuOption === 0) {
        dispatch(fetchPokemon(chosenPokemon.label.toLowerCase()));
      } else {
        dispatch(fetchPokemon(getRandomInt(1, maxPokemonNumber + 1)));
      }
    }
  }, [status, dispatch]);

  // Pokemon information card
  function renderPokemonCard() {
    return (
      <Card raised={true} style={{ borderRadius: 40 }}>
        <CardHeader title={pokemonInfo.name.toUpperCase()} />
        <CardMedia>
          <div>
            <img
              src={pokemonInfo.sprites.front_default}
              style={{ height: 150 }}
              alt={pokemonInfo.name.toUpperCase()}
            />
            <img
              src={pokemonInfo.sprites.back_default}
              style={{ height: 150 }}
              alt={pokemonInfo.name.toUpperCase()}
            />
          </div>
        </CardMedia>
        <CardContent>
          <Grid container spacing={3}>
            <Grid item sm={3}>
              <h4>Weight:</h4>
              {pokemonInfo.weight}
            </Grid>

            <Grid item sm={3}>
              <h4>Height:</h4>
              {pokemonInfo.height}
            </Grid>

            <Grid item sm={3}>
              <h4>Types:</h4>
              {pokemonInfo.types.map((type) => (
                <div key={type.type.name}>{type.type.name.toUpperCase()}</div>
              ))}
            </Grid>

            <Grid item sm={3}>
              <h4>Abilities:</h4>
              {pokemonInfo.abilities.map((ability) => (
                <div key={ability.ability.name}>
                  {ability.ability.name.toUpperCase()}
                </div>
              ))}
            </Grid>
          </Grid>
        </CardContent>
      </Card>
    );
  }

  // Render pokemon unformation page
  function renderPokemonInfoPage() {
    if (status === "loading") {
      return <CircularProgress />;
    } else if (status === "failed" || error != null) {
      return (
        <Alert severity="error">
          <AlertTitle>Error</AlertTitle>
          This is an error alert — <strong>{error}</strong>
        </Alert>
      );
    } else if (status === "succeeded") {
      return renderPokemonCard();
    }
  }

  // Full component render
  return <div>{renderPokemonInfoPage()}</div>;
}
