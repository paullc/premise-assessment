import React from "react";
import { PokemonInfoPage } from "../pokemonInfoPage/PokemonInfoPage";
import { useSelector } from "react-redux";
import { selectSelectedOption } from "../menuPage/menuPageSlice";

// This component is the final page in the stepper. It summarizes interesting information about a Pokemon
export function SummaryPage() {
  // The selected menu option -> [0, 1] -> [searched, random]
  const menuOption = useSelector(selectSelectedOption);

  // Render the page according to which menu option was picked
  // Not needed for thi simple app but would help for future improvements/expansions
  function renderSummaryPage(selectedOption) {
    switch (selectedOption) {
      case 0:
        return <PokemonInfoPage />;

      // This is redundant but I am leaving it so that it can be expanded at a later date if
      // i want to create different summary pages.
      case 1:
        return <PokemonInfoPage />;
      default:
        return <div>Error</div>;
    }
  }

  // Render summary page
  return <div>{renderSummaryPage(menuOption)}</div>;
}
