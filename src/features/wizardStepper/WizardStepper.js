import * as React from "react";
import {
  Box,
  Stepper,
  Step,
  StepLabel,
  Button,
  Typography,
} from "@material-ui/core";

import { MenuPage } from "../menuPage/MenuPage";
import { OptionsPage } from "../optionsPage/OptionsPage";
import { SummaryPage } from "../summaryPage/SummaryPage";
import { useSelector, useDispatch } from "react-redux";
import {
  selectActiveStep,
  setActiveStep,
  nextStep,
  backStep,
  resetActiveStep,
} from "./wizardStepperSlice";
import { selectSelectedOption } from "../menuPage/menuPageSlice";
import { selectPokemonInfo } from "../pokemonInfoPage/pokemonInfoPageSlice";

// The iterable steps that compose the stepper
const steps = [
  "What would you like to learn about?",
  "Pokémon name",
  "Summary",
];

// This component is all that resides within the functionality of the stepper.
// All the next, back, and state logic is container here or in child components.
export default function WizardStepper() {
  // The current active stepper step
  const activeStep = useSelector(selectActiveStep);
  const dispatch = useDispatch();
  // The selected menu option -> [0, 1] -> [searched, random]
  const menuOption = useSelector(selectSelectedOption);

  // The info about the currently selected pokemon. This is placed here so that the Finish button
  // has access to the data that we need to Console.log() at the end.
  const pokemonInfo = useSelector(selectPokemonInfo);

  // Checks if a step is optional
  const isStepOptional = (step) => {
    return step === 1;
  };

  // Render the stepper page depending on what the active step is
  function renderPage(pageNumber) {
    switch (pageNumber) {
      case 0:
        return <MenuPage />;
      case 1:
        return <OptionsPage />;
      case 2:
        return <SummaryPage />;
      default:
        return <div>Error</div>;
    }
  }

  // Render the stepper component
  return (
    <Box sx={{ width: "100", padding: 50 }}>
      <Stepper activeStep={activeStep}>
        {steps.map((label, index) => {
          const stepProps = {};
          const labelProps = {};
          if (isStepOptional(index)) {
            labelProps.optional = (
              <Typography variant="caption">Optional</Typography>
            );
          }

          return (
            <Step key={label} {...stepProps}>
              <StepLabel {...labelProps}>{label}</StepLabel>
            </Step>
          );
        })}
      </Stepper>
      {activeStep === steps.length ? (
        <React.Fragment>
          <Typography sx={{ mt: 2, mb: 1 }}>
            Thanks for using my wizard! Click Reset to start again!
          </Typography>
          <Box sx={{ display: "flex", flexDirection: "row", pt: 2 }}>
            <Box sx={{ flex: "1 1 auto" }} />
            <Button
              color="inherit"
              disabled={activeStep === 0}
              onClick={() => {
                if (menuOption === 0) {
                  dispatch(backStep());
                } else {
                  dispatch(setActiveStep(0));
                }
              }}
              sx={{ mr: 1 }}
            >
              Back
            </Button>
            <Button onClick={() => dispatch(resetActiveStep())}>Reset</Button>
          </Box>
        </React.Fragment>
      ) : (
        <React.Fragment>
          <Box sx={{ display: "flex", flexDirection: "column", pt: 2 }}>
            {renderPage(activeStep)}
            <Box sx={{ display: "flex", flexDirection: "row", pt: 2 }}>
              <Button
                color="inherit"
                disabled={activeStep === 0}
                onClick={() => {
                  if (menuOption === 0) {
                    dispatch(backStep());
                  } else {
                    dispatch(setActiveStep(0));
                  }
                }}
                sx={{ mr: 1 }}
              >
                Back
              </Button>
              <Box sx={{ flex: "1 1 auto" }} />

              {activeStep === steps.length - 1 && (
                <Button
                  onClick={() => {
                    dispatch(nextStep());
                    console.log({
                      pokemonName: pokemonInfo.name,
                      randomlySelected: menuOption === 1,
                      pokemonTypes: pokemonInfo.types,
                      height: pokemonInfo.height,
                      weight: pokemonInfo.weight,
                      abilities: pokemonInfo.abilites,
                    });
                  }}
                >
                  Finish
                </Button>
              )}
            </Box>
          </Box>
        </React.Fragment>
      )}
    </Box>
  );
}
