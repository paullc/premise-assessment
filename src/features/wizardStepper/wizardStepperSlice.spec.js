import wizardStepperReducer, {
  nextStep,
  backStep,
  resetActiveStep,
  setActiveStep,
} from "./wizardStepperSlice";

describe("wizard stepper reducer", () => {
  const initialState = {
    activeStep: 0,
  };

  it("should handle initial state", () => {
    expect(wizardStepperReducer(undefined, { type: "unknown" })).toEqual({
      activeStep: 0,
    });
  });

  it("should handle next step", () => {
    const actual = wizardStepperReducer(initialState, nextStep());
    expect(actual.activeStep).toEqual(1);
  });
  it("should handle back step", () => {
    const actual = wizardStepperReducer(initialState, backStep());
    expect(actual.activeStep).toEqual(-1);
  });

  it("should handle reset active step", () => {
    const actual = wizardStepperReducer(initialState, resetActiveStep());
    expect(actual.activeStep).toEqual(0);
  });

  it("should handle set active step", () => {
    const actual = wizardStepperReducer(initialState, setActiveStep(30));
    expect(actual.activeStep).toEqual(30);
  });
});
