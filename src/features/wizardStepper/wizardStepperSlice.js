import { createSlice } from "@reduxjs/toolkit";

const initialState = {
  activeStep: 0,
};

export const wizardStepperSlice = createSlice({
  name: "wizardStepper",
  initialState, // The `reducers` field lets us define reducers and generate associated actions
  reducers: {
    nextStep: (state) => {
      state.activeStep += 1;
    },
    backStep: (state) => {
      state.activeStep -= 1;
    },
    resetActiveStep: (state) => {
      state.activeStep = 0;
    },
    setActiveStep: (state, action) => {
      state.activeStep = action.payload;
    },
  },
});

// Selectors
export const selectActiveStep = (state) => state.wizardStepper.activeStep;

// Actions

export const { nextStep, backStep, resetActiveStep, setActiveStep } =
  wizardStepperSlice.actions;

// Reducer

export default wizardStepperSlice.reducer;
