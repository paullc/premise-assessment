import { createAsyncThunk, createSlice } from "@reduxjs/toolkit";
import axios from "axios";

const initialState = {
  pokemon: [],
  chosenPokemon: null,
  status: "idle",
  error: null,
};

// An async thunk that grabs all the names of every existing pokemon so that they can populate the auto-complete
// form. The result JSON is restructured to match the Material-UI API for the AutoComplete component
export const fetchAllPokemon = createAsyncThunk(
  "pokemonSearchPage/fetchAllPokemon",
  async () => {
    return await axios
      .get("https://pokeapi.co/api/v2/pokemon/?limit=2000")
      .then((response) => {
        let newResults = [];
        let id = 0;
        response.data.results.forEach((result) => {
          let capitalized =
            result.name[0].toUpperCase() +
            result.name.substring(1).toLowerCase();

          newResults.push({
            label: capitalized,
            id: id++,
          });
        });
        return newResults;
      })
      .catch((error) => error);
  }
);

export const pokemonSearchPageSlice = createSlice({
  name: "pokemonSearchPage",
  initialState, // The `reducers` field lets us define reducers and generate associated actions
  reducers: {
    choosePokemon: (state, action) => {
      state.chosenPokemon = action.payload;
    },
    loadPokemonList: (state, action) => {
      state.pokemon = action.payload;
    },
  },
  extraReducers: (builder) => {
    builder
      .addCase(fetchAllPokemon.pending, (state, action) => {
        state.status = "loading";
      })
      .addCase(fetchAllPokemon.fulfilled, (state, action) => {
        state.status = "succeeded";
        // Add any fetched posts to the array
        state.pokemon = action.payload;
        state.chosenPokemon = action.payload.find((elem) => {
          return elem.label === "Arbok";
        });
      })
      .addCase(fetchAllPokemon.rejected, (state, action) => {
        state.status = "failed";
        state.error = action.error.message;
      });
  },
});

// Selectors
export const selectAllPokemon = (state) => state.pokemonSearchPage.pokemon;
export const selectChosenPokemon = (state) =>
  state.pokemonSearchPage.chosenPokemon;
export const selectFetchStatus = (state) => state.pokemonSearchPage.status;
export const selectError = (state) => state.pokemonSearchPage.error;

// Actions

export const { choosePokemon, loadPokemonList } =
  pokemonSearchPageSlice.actions;

// Reducer

export default pokemonSearchPageSlice.reducer;
