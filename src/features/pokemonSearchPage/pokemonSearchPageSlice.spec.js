import pokemonSearchPageReducer, {
  choosePokemon,
  loadPokemonList,
} from "./pokemonSearchPageSlice";

describe("pokemon search page reducer", () => {
  const initialState = {
    pokemon: [],
    chosenPokemon: null,
    status: "idle",
    error: null,
  };

  it("should handle initial state", () => {
    expect(pokemonSearchPageReducer(undefined, { type: "unknown" })).toEqual({
      pokemon: [],
      chosenPokemon: null,
      status: "idle",
      error: null,
    });
  });

  it("should handle choose pokemon", () => {
    const actual = pokemonSearchPageReducer(
      initialState,
      choosePokemon({ id: 10 })
    );
    expect(actual.chosenPokemon).toEqual({ id: 10 });
  });
  it("should handle load pokemon list 1", () => {
    const actual = pokemonSearchPageReducer(
      initialState,
      loadPokemonList([{ id: 1 }, { id: 2 }])
    );
    expect(actual.pokemon).toEqual([{ id: 1 }, { id: 2 }]);
  });

  it("should handle load pokemon list 2", () => {
    const actual = pokemonSearchPageReducer(
      initialState,
      loadPokemonList(["this could be a pokemon list"])
    );
    expect(actual.pokemon[0]).toEqual("this could be a pokemon list");
  });
});
