import React, { useEffect } from "react";
import { Autocomplete, TextField, Grid, Button, Box } from "@mui/material";
import {
  selectAllPokemon,
  selectChosenPokemon,
  fetchAllPokemon,
  selectFetchStatus,
  choosePokemon,
} from "./pokemonSearchPageSlice";
import { useDispatch, useSelector } from "react-redux";
import { nextStep } from "../wizardStepper/wizardStepperSlice";
import { newSearch } from "../pokemonInfoPage/pokemonInfoPageSlice";

// The search page contains all logic needed for Pokemon searching
export function PokemonSearchPage() {
  const dispatch = useDispatch();

  // List of all pokemon names
  const pokemonList = useSelector(selectAllPokemon);

  // Status of action, used in order to send the first API query
  const fetchStatus = useSelector(selectFetchStatus);

  // The pokemon chosen by the user or set as default at start (Arbok)
  const chosenPokemon = useSelector(selectChosenPokemon);

  // Options styling and organizing for the auto-complete dropdown
  const options = pokemonList.map((option) => {
    const firstLetter = option.label[0].toUpperCase();
    return {
      firstLetter: /[0-9]/.test(firstLetter) ? "0-9" : firstLetter,
      ...option,
    };
  });

  // This is where we fetch all the pokemon names upon render.
  useEffect(() => {
    if (fetchStatus === "idle") {
      dispatch(fetchAllPokemon());
    }
  }, [fetchStatus, dispatch]);

  // A small helper function which resets the pokemon details page so that
  // it reflects the user's most recent pokemon selection
  function dispatchSearch() {
    dispatch(newSearch());
    dispatch(nextStep());
  }

  // Render pokemon search page
  return (
    <div className="container">
      <Grid container spacing={3}>
        <Grid item sm={3} />

        <Grid item sm={6}>
          <Box sx={{ display: "flex", flexDirection: "row", pt: 2 }}>
            <Autocomplete
              isOptionEqualToValue={(option, value) => option.id === value.id}
              disableClearable={true}
              value={chosenPokemon}
              onChange={(event, newValue) => {
                dispatch(choosePokemon(newValue));
              }}
              options={options.sort(
                (a, b) => -b.firstLetter.localeCompare(a.firstLetter)
              )}
              groupBy={(option) => option.firstLetter}
              sx={{ width: 300, marginX: 5 }}
              renderInput={(params) => (
                <TextField {...params} label="Pokemon" />
              )}
            />
            <Button onClick={() => dispatchSearch()}>Search</Button>
          </Box>
        </Grid>

        <Grid item sm={3} />
      </Grid>
    </div>
  );
}
