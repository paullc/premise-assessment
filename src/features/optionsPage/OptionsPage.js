import React from "react";
import { useSelector } from "react-redux";
import { selectSelectedOption } from "../menuPage/menuPageSlice";
import { PokemonSearchPage } from "../pokemonSearchPage/PokemonSearchPage";

// The options page serves as a page where a decision can be further refined or filtered. This is the case
// for the user when they click the Search Pokemon menu option. This is the page that is also skipped when
// a users opts to randomly research a pokemon.
export function OptionsPage() {
  // Selected menu option -> [0, 1] -> [search, random]
  const menuOption = useSelector(selectSelectedOption);

  // Separating out some of the rendering in order to de-clutter
  function renderOptionsPage(selectedOption) {
    // Leaving this as a switch in case I wish to add more option pages later (would make sense if i created
    // a few new menu option that required a different path through the form.
    switch (selectedOption) {
      case 0:
        return <PokemonSearchPage />;
      default:
        return <div>Error</div>;
    }
  }

  // Rendered components
  return <div>{renderOptionsPage(menuOption)}</div>;
}
