import { createSlice } from "@reduxjs/toolkit";

const initialState = {
  selectedOption: -1,
};

export const menuPageSlice = createSlice({
  name: "menuPage",
  initialState,
  reducers: {
    setOption: (state, action) => {
      state.selectedOption = action.payload;
    },
    resetOption: (state) => {
      state.selectedOption = -1;
    },
  },
});

// Selectors
export const selectSelectedOption = (state) => state.menuPage.selectedOption;

// Actions

export const { setOption, resetOption } = menuPageSlice.actions;

// Reducer

export default menuPageSlice.reducer;
