import menuPageReducer, { setOption, resetOption } from "./menuPageSlice";

describe("menu page reducer", () => {
  const initialState = {
    selectedOption: -1,
  };

  it("should handle initial state", () => {
    expect(menuPageReducer(undefined, { type: "unknown" })).toEqual({
      selectedOption: -1,
    });
  });

  it("should handle setOption", () => {
    const actual = menuPageReducer(initialState, setOption(4));
    expect(actual.selectedOption).toEqual(4);
  });

  it("should handle resetOption", () => {
    const actual = menuPageReducer(initialState, resetOption());
    expect(actual.selectedOption).toEqual(-1);
  });
});
