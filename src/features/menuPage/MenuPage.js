import {
  Grid,
  Card,
  CardHeader,
  CardContent,
  CardMedia,
  Typography,
  ButtonBase,
  CardActionArea,
} from "@material-ui/core/";
import React from "react";
import pikachuImg from "../../resources/pikachu.png";
import randomImg from "../../resources/random.png";
import {
  selectActiveStep,
  setActiveStep,
  nextStep,
  backStep,
  resetActiveStep,
} from "../wizardStepper/wizardStepperSlice";
import { setOption } from "./menuPageSlice";
import { useDispatch } from "react-redux";
import { newSearch } from "../pokemonInfoPage/pokemonInfoPageSlice";

// The menu page holds a grid of clickable cards that allow the user to choose
// how they want to use the wizard: Either to search for a specific Pokemon, or randomly be shown one.
export function MenuPage() {
  const dispatch = useDispatch();

  // Actions depending on clicked option
  // Option 0 leads to a Pokemon search, option 1 leads to a random pokemon
  function clickedOption(number) {
    dispatch(setOption(number));
    if (number === 0) {
      dispatch(nextStep());
    } else {
      dispatch(newSearch());
      dispatch(setActiveStep(2));
    }
  }

  // Card for each menu choice

  function menuCard(title, image, imageTitle, content, optionNumber) {
    return (
      <Card raised={true} style={{ borderRadius: 40 }}>
        <CardActionArea onClick={() => clickedOption(optionNumber)}>
          <CardHeader title={title} />
          <CardMedia>
            <img src={image} style={{ height: 300 }} alt={"Missing image"} />
          </CardMedia>
          <CardContent>{content}</CardContent>
        </CardActionArea>
      </Card>
    );
  }

  // Rendered component

  return (
    <div className="optionContainer">
      <Grid container spacing={3}>
        <Grid item sm={6}>
          {menuCard(
            "A Specific Pokémon",
            pikachuImg,
            "Pikachu",
            "Research a specific Pokémon",
            0
          )}
        </Grid>

        <Grid item sm={6}>
          {menuCard(
            "A Random Pokémon",
            randomImg,
            "Random Pokémon",
            "Research a random Pokémon",
            1
          )}
        </Grid>
      </Grid>
    </div>
  );
}
